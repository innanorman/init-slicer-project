import Image from 'next/image'
import React from 'react'

const Header = () => {
  return (
    <header className='bg-white w-full px-[62px] py-[40px]'>
			<nav className="mx-auto flex items-center justify-between" aria-label="Global">
				<div className="flex">
					<a href="#" className="-m-1.5 p-1.5">
						<span className="sr-only"> Your Company </span>
						{/* <img className="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt=""> */}
						<Image
							src="/logo-brand.png"
							width={100}
							height={100}
							alt="Picture of the author"
						/>
					</a>
				</div>

				<div className='flex justify-between gap-x-8'>
				<button type="button" className="flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900" aria-expanded="false">
						Tentang Kami
						<svg className="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
						</svg>
					</button>
					<button type="button" className="flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900" aria-expanded="false">
					Produk &amp; Layanan
						<svg className="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
						</svg>
					</button>
					<a href="#" className="text-sm font-semibold leading-6 text-gray-900">Blog</a>
					<a href="#" className="text-sm font-semibold leading-6 text-gray-900">FAQ</a>

				</div>

				<div className='flex justify-between'>
					<a href="#" className="text-sm font-semibold leading-6 text-gray-900 px-4 py-2.5">Daftar</a>
					<button type="button" className="flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900 rounded-md px-4 py-2.5 bg-[#F5333F]" aria-expanded="false">
						Masuk
						{/* <svg className="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
						</svg> */}
					</button>
				</div>
			</nav>
    </header>
  )
}

export default Header