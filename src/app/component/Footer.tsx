import Image from 'next/image'
import React from 'react'

const Footer = () => {

	return (
		<div className='bg-[#012846] px-16 py-[39px] w-full'>
				<div className='flex justify-between gap-x-[149px] mb-[46px]'>
					<div className='w-1/2'>
						<Image
							src="/logo-brand-white.png"
							width={160}
							height={100}
							alt="Picture of the author"
						/>
						<div className='text-white my-4'>
								SobatBangun adalah platform digital dari SIG yang bergerak dengan misi mengembangkan proses pembangunan dan renovasi rumah secara lebih baik serta berkelanjutan.
						</div>
						<div className='flex text-white mb-2'>
				
								<Image
									src="/icon-message.png"
									width={24}
									height={24}
									alt="Icon Message"
									style={{ padding: '4px'}}
								/>

							<span className='text-white'>sobat@sobatbangum.com</span>
						</div>
						<div className='social-media-section'>
							<div className='py-2 text-white'>Sosial Media :</div>
						<div className='social-media-wrapper flex gap-x-[26px]'>
							<div className='background-icon bg-[#2C373E] flex justify-center items-center'>
								<Image
									src="/icon/icon-instagram.png"
									width={18}
									height={18}
									alt="Icon Message"
									// style={{ padding: '4px'}}
								/>
							</div>
							<div className='background-icon bg-[#fff] flex justify-center items-center'>
								<Image
									src="/icon/icon-fb.png"
									width={14}
									height={14}
									alt="Icon Message"
									// style={{ padding: '4px'}}
								/>
							</div>
							<div className='background-icon bg-[#fff] flex justify-center items-center'>
								<Image
									src="/icon/icon-ytb.png"
									width={24}
									height={24}
									alt="Icon Message"
									// style={{ padding: '4px'}}
								/>
							</div>
						</div>
						</div>
						
					</div>
					<div className='w-1/2 flex text-white gap-x-12'>
						<div className=''>
							<p className='font-bold font-xl mb-6'>Produk & Layanan</p>
							<ul className='gap-y-2'>
								<li className='mb-2'>Renovasi</li>
								<li className='mb-2'>Bangun Rumah</li>
								<li className='mb-2'>Layanan Desain</li>
								<li className='mb-2'>Teknologi Tambahan</li>
								<li>Beli Material</li>
							</ul>
						</div>

						<div className=''>
							<p className='font-bold font-xl mb-6'>Tentang Kami</p>
							<ul>
								<li className='mb-2'>Tentang SobatBangun</li>
								<li className='mb-2'>Kebijakan Dan Privasi</li>
								<li className='mb-2'>Syarat Dan Ketentuan</li>
								<li className='mb-2'>FAQ</li>
								<li>Daftar Menjadi Mitra</li>
							</ul>
						</div>

					</div>
				</div>
				<div className='flex justify-between'>
					<div className='group-icon-wrapper'>
						<p className='font-bold text-white'>Kredit Bangun Rumah</p>
						
						<div className='items-center flex justify-between'>
							<Image
								src="/logo-bank/logo-mandiri.png"
								width={56}
								height={56}
								alt="mandiri"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-btn.png"
								width={56}
								height={56}
								alt="btn"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-bni.png"
								width={56}
								height={56}
								alt="bni"
								// style={{ padding: '4px'}}
							/>
						</div>
					</div>

					<div className='group-icon-wrapper'>
						<p className='font-bold text-white'>Tunai Via Bank Transfer</p>
						
						<div className='items-center flex justify-between'>
							<Image
								src="/logo-bank/logo-mandiri.png"
								width={56}
								height={56}
								alt="mandiri"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-bri.png"
								width={56}
								height={56}
								alt="bri"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-bni.png"
								width={56}
								height={56}
								alt="bni"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-permata.png"
								width={56}
								height={56}
								alt="permata"
								// style={{ padding: '4px'}}
							/>
						</div>
					</div>

					<div className='group-icon-wrapper'>
						<p className='font-bold text-white'>Kartu Kredit</p>
						
						<div className='items-center flex justify-between'>
							<Image
								src="/logo-bank/logo-visa.png"
								width={56}
								height={56}
								alt="visa"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-mastercard.svg"
								width={56}
								height={56}
								alt="mastercard"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/logo-jcb.svg"
								width={56}
								height={56}
								alt="jcb"
								// style={{ padding: '4px'}}
							/>
						</div>
					</div>

					<div className='group-icon-wrapper'>
						<p className='font-bold text-white'>Rekan Teknologi Tambahan</p>
						
						<div className='items-center flex justify-between'>
							<Image
								src="/logo-bank/rekan-tekno-1.png"
								width={56}
								height={56}
								alt="asco"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/rekan-tekno-2.png"
								width={56}
								height={56}
								alt="inecosolar"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/rekan-tekno-3.png"
								width={56}
								height={56}
								alt="adidaya"
								// style={{ padding: '4px'}}
							/>

							<Image
								src="/logo-bank/rekan-tekno-4.png"
								width={56}
								height={56}
								alt="agrasuryaenergi"
								// style={{ padding: '4px'}}
							/>
						</div>
					</div>
				</div>

				<div className='flex justify-between'>
					 <div>
						Powered by :
						<Image
							src="/logo-sig.png"
							width={56}
							height={56}
							alt="mandiri"
							// style={{ padding: '4px'}}
						/>

					 </div>
					 <div>
					 Copyright © 2023 SobatBangun. All rights reserved.
					 </div>
				</div>

		</div>
	)
}

export default Footer