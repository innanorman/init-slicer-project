import Image from 'next/image'
import React from 'react'

const DetailCard = () => {
		const dataDetail = {
				name: 'Omah Apik 3',
				studio: 'Studio SAe',
				iconStudio: '/logo-studio.png',
				jenisRumah: 'Scandinavian',
				tipeDesign: 'Dapat Dimodifikasi',
				detailBangunan: [
						{
								title: 'Dimensi Tanah',
								icon: '/icon/icon-dimensi.png',
								content: '15 x 8m'
						},
						{
								title: 'Luas Bangunan',
								icon: '/icon/icon-luas.png',
								content: '112m2'
						},
						{
								title: 'Lantai',
								icon: '/icon/icon-lantai.png',
								content: '2'
						},
						{
								title: 'Kamar Tidur',
								icon: '/icon/icon-kamar.png',
								content: '4' 
						},
						
				],
				hargaDesain: 'Rp. 32.500.000',
				hargaKontruksi: 'Rp 560.000.000'
		}
	return (
		<div className='p-4 rounded border border-[#E6E6E6] box-border'>
				<div className='title-section'>
						<div className='text-color-[#1A1A1A] text-2xl mb-2'>{dataDetail?.name}</div>
					 <div className='flex'>
						<Image
								src={dataDetail?.iconStudio}
								width={28}
								height={28}
								alt={dataDetail?.name}
								// style={{ padding: '4px'}}
						/>
						<span>{dataDetail?.studio}</span>
					 </div>

					 <div className=''>
								<div className='flex gap-x-6 text-sm mb-2'>
										<div className='w-20 text-color-[#4D4D4D]'>Jenis Rumah</div>
										<div>{dataDetail?.jenisRumah}</div>
								</div>
								<div className='flex gap-x-6 text-sm'>
										<div className='w-20 text-[#4D4D4D]'>Tipe Desain</div>
										<div className='flex items-center'>
												<Image
														src="/icon/icon-checked.png"
														width={16}
														height={16}
														alt={dataDetail?.name}
														// style={{ padding: '4px'}}
												/>
												<span className='text-[#F5333F] ml-4'>{dataDetail?.tipeDesign}</span>
										</div>
										
								</div>
								<hr className="my-4 h-0.5 border-t-0 bg-neutral-100 dark:bg-white/10" />
					 </div>
				</div>
				<div className='info-section'>
						<div className='flex items-center justify-between'>
								{dataDetail?.detailBangunan.map(item => (
										<div className='text-xs flex flex-col justify-center items-center'>
												{item.title}
												<Image
														src={item.icon}
														width={24}
														height={24}
														alt={dataDetail?.name}
														// style={{ padding: '4px'}}
												/>
												{item.content}

										</div>
								))}
						</div>
						<hr className="my-4 h-0.5 border-t-0 bg-neutral-100 dark:bg-white/10" />

				</div>
				<div className='price-section gap-y-1'>
						<div className='text-sm'>Harga Desain</div>
						<div className='text-[32px] font-medium'>
							{dataDetail?.hargaDesain}
						</div>
						<div className='text-[#808080]'>
						Harga konstruksi mulai dari {dataDetail?.hargaKontruksi}
						</div>
						
				</div>
				
					<button type="button" className="flex items-center w-full align-center gap-x-1 text-sm font-semibold leading-6 text-gray-900 rounded-md px-4 py-2.5 bg-[#F5333F] text-white" aria-expanded="false">
						Konsultasi Sekarang
						{/* <svg className="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
						</svg> */}
					</button>
				

		</div>
	)
}

export default DetailCard