import Image from 'next/image'
import React from 'react'
import DetailCard from './DetailCard'

const DetailProduct = () => {
    const dummyData = [
        {
            id: 1, 
            name: "Ruang Keluarga",
            area: "2.0 x 2.9",
            image: '/ruang-kerja.png'
        },
        {
            id: 2, 
            name: "Kamar tidur",
            area: "4.0 x 3.4",
            image: '/kamar-tidur.png'
        },
        {
            id: 3, 
            name: "Ruang Makan & Dapur",
            area: "3.0 x 2.9",
            image: '/ruang-makan.png'
        },
        {
            id: 4, 
            name: "Ruang Kerja",
            area: "2.0 x 2.9",
            image: '/ruang-kerja.png'
        },
        {
            id: 5, 
            name: "Kamar tidur",
            area: "4.0 x 3.4",
            image: '/kamar-tidur.png'
        }
    ]
  return (
    <div className='bg-white mt-10 w-full px-[62px]'>
        <div className='flex justify-between'>
            <div className='w-9/12 flex flex-wrap gap-6'>
                {dummyData.map(isi => 
                    <div className='p-4 rounded border border-[#E6E6E6] box-border'>
                        <Image
                            src={isi.image}
                            width={262}
                            height={201}
                            alt={isi?.name}
                            // style={{ padding: '4px'}}
                        />

                        <div className='gap-y-2 mt-6'>
                        <p className='text-xl font-bold text-[#1A202C]'>{isi?.name}</p>
                        <p> {isi?.area}</p>
                        </div>
                        
                    </div>
                )}
            </div>
            <div className='w-3/12'>
                <DetailCard />
            </div>
        </div>
        
    </div>
  )
}

export default DetailProduct